﻿using UnityEngine;

namespace LD44
{
    public class UiIntroController : MonoBehaviour
    {
        [SerializeField] private Animator animator = null;

        [SerializeField] private AudioClip bangSound = null;

        public void Awake()
        {
            animator.SetTrigger("Intro");
        }

        public void PlayBang()
        {
            AudioSource.PlayClipAtPoint(bangSound, Vector3.zero);
        }

        public void FinishIntro()
        {
            GameController.Instance.GoToScene(SceneName.Insurance);
        }
    }
}