﻿using UnityEngine;
using UnityEngine.UI;

namespace LD44
{
    public class UiInsuranceController : MonoBehaviour
    {
        [SerializeField] private Animator animator = null;

        [SerializeField] private GameObject contractBuy = null;
        [SerializeField] private GameObject contractTrauma = null;

        [SerializeField] private Image mustWearItemImage = null;

        [SerializeField] private Button buyButton = null;
        [SerializeField] private Button okButton = null;

        [SerializeField] private Text changeMoneyValueText = null;
        [SerializeField] private Text moneyValueText = null;

        [SerializeField] private AudioClip bangSound = null;

        public void Awake()
        {
            if (!GameVariables.Instance.HasContract)
            {
                contractBuy.SetActive(true);
                animator.SetTrigger("ShowBuyContract");
                moneyValueText.text = $"{GameVariables.Instance.Money} $";

                buyButton.onClick.AddListener(() =>
                {
                    GameVariables.Instance.HasContract = true;
                    GameVariables.Instance.Money -= GameVariables.Instance.ContactCost;
                    changeMoneyValueText.text = $"- {GameVariables.Instance.ContactCost} $";
                    animator.SetTrigger("ChangeMoney");
                    buyButton.interactable = false;
                });
            }
            else
            {
                contractTrauma.SetActive(true);
                var traumaResult = GameVariables.Instance.GetTraumaResultByTraumaType(GameVariables.Instance.LastTraumaType);
                GameVariables.Instance.MustWearItemTypes.Add(traumaResult.MustWearItemType);
                mustWearItemImage.sprite = traumaResult.Sprite;
                animator.SetTrigger("ShowTraumaContract");

                okButton.onClick.AddListener(() =>
                {
                    GameVariables.Instance.Money += traumaResult.Compensation;
                    changeMoneyValueText.text = $"+ {traumaResult.Compensation} $";
                    animator.SetTrigger("ChangeMoney");
                    okButton.interactable = false;
                });
            }
        }

        public void PlayBang()
        {
            AudioSource.PlayClipAtPoint(bangSound, Vector3.zero);
        }

        public void UpdateMoneyValueText()
        {
            moneyValueText.text = $"{GameVariables.Instance.Money} $";
        }

        public void FinishInsurance()
        {
            GameController.Instance.GoToScene(SceneName.ConstructionSite);
        }
    }
}