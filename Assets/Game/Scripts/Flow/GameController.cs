﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace LD44
{
    public class GameController : MonoBehaviour
    {
        [SerializeField] private PrototypeLoockup prototypesLoockup = null;
        [SerializeField] private Image fadeCurtain = null;

        private GameStateController gameStateController = null;
        private string stateSceneName = string.Empty;

        public static GameController Instance = null;

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
        }

        void Start()
        {
            prototypesLoockup.Initialize();
            InitializeScene();
            //stateSceneName = SceneName.Insurance;
            stateSceneName = SceneName.ConstructionSite;
            GameVariables.Instance.HasContract = true;
            //if (stateSceneName == string.Empty)
            //{
            //    StartCoroutine(LoadSceneCoroutine(SceneName.Intro));
            //}
        }

        void Update()
        {
            if (gameStateController != null)
            {
                gameStateController.Run();
            }
        }

        void FixedUpdate()
        {
            if (gameStateController != null)
            {
                gameStateController.FixedRun();
            }
        }

        void OnDrawGizmos()
        {
            if (gameStateController != null)
            {
                gameStateController.DrawGizmos();
            }
        }

        public void GoToScene(string sceneName)
        {
            StartCoroutine(LoadSceneCoroutine(sceneName));
        }

        IEnumerator LoadSceneCoroutine(string sceneName)
        {
            if (stateSceneName != string.Empty)
            {
                fadeCurtain.enabled = true;
                while (fadeCurtain.color.a < 0.9999f)
                {
                    fadeCurtain.color = new Color(
                        fadeCurtain.color.r,
                        fadeCurtain.color.b,
                        fadeCurtain.color.b,
                        fadeCurtain.color.a + Time.deltaTime * 3.0f);
                    yield return null;
                }

                var asyncUnloadOperation = SceneManager.UnloadSceneAsync(stateSceneName);

                while (!asyncUnloadOperation.isDone)
                {
                    Debug.Log("Unloading...");
                    yield return null;
                }
            }

            yield return new WaitForSeconds(0.15f);

            var asyncLoadOperation = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);

            while (!asyncLoadOperation.isDone)
            {
                Debug.Log("Loading");
                yield return null;
            }

            while (fadeCurtain.color.a > 0.001f)
            {
                fadeCurtain.color = new Color(
                    fadeCurtain.color.r,
                    fadeCurtain.color.b,
                    fadeCurtain.color.b,
                    fadeCurtain.color.a - Time.deltaTime * 3.0f);
                yield return null;
            }

            fadeCurtain.enabled = false;

            stateSceneName = sceneName;
            InitializeScene();

            yield return null;
        }

        private void InitializeScene()
        {
            if (gameStateController != null)
            {
                gameStateController.Shutdown();
            }

            gameStateController = FindObjectOfType<GameStateController>();
            if (gameStateController == null)
            {
                throw new UnityException("Game state not found");
            }

            var poolCapacitiesProvider = gameStateController.GetComponent<PoolCapacitiesProvider>();
            if (poolCapacitiesProvider == null)
            {
                throw new UnityException("Pool capacities provider not found");
            }

            gameStateController.Initialize(prototypesLoockup, poolCapacitiesProvider.GetPoolCapacities());
        }
    }
}
