﻿namespace LD44
{
    public static class SceneName
    {
        public const string Intro = "Intro";
        public const string ConstructionSite = "ConstructionSite";
        public const string Insurance = "Insurance";
    }
}
