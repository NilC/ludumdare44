﻿using System.Collections;
using UnityEngine;

namespace LD44
{
    public class CoroutineHelper : MonoBehaviour
    {
        public static CoroutineHelper Instance = null;

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
        }

        public Coroutine PlayCoroutine(IEnumerator routine)
        {
            return StartCoroutine(routine);
        }
    }
}
