﻿using UnityEngine;

namespace LD44
{
    public abstract class GameStateController : MonoBehaviour
    {
        protected EntityFactory EntityFactory = null;

        public abstract void Initialize(PrototypeLoockup prototypesLoockup, PoolCapacity[] poolCapacities);
        public abstract void Shutdown();
        public abstract void Run();
        public abstract void FixedRun();

#if UNITY_EDITOR
        public virtual void DrawGizmos()
        {
        }
#endif
    }
}
