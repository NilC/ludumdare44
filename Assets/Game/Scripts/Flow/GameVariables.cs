﻿using System.Collections.Generic;
using UnityEngine;

namespace LD44
{
    public class GameVariables : MonoBehaviour
    {
        public int Money = 100;
        public int ContactCost = 100;
        public int TotalCompensation = 10000;
        public TraumaType LastTraumaType = TraumaType.None;
        public bool HasContract = false;
        public List<MustWearItemType> MustWearItemTypes = new List<MustWearItemType>(0);

        [SerializeField] private TraumaResult[] traumaResults = new TraumaResult[0];

        public static GameVariables Instance = null;

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
        }

        public TraumaResult GetTraumaResultByTraumaType(TraumaType traumaType)
        {
            for (int i = 0; i < traumaResults.Length; i++)
            {
                if (traumaResults[i].TraumaType == traumaType)
                {
                    return traumaResults[i];
                }
            }

            throw new UnityException($"Can not find trauma result for trauma type {traumaType}");
        }
    }
}
