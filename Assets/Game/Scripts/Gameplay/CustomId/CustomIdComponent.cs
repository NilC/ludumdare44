﻿using UnityEngine;

namespace LD44
{
    public class CustomIdComponent : MonoBehaviour, IComponent
    {
        public int CustomId;

        public int EntityId { private set; get; }

        public void Awake()
        {
            EntityId = gameObject.GetEntityId();
        }

        public void OnEnable()
        {
            Systems.CustomId.AddComponent(gameObject.GetEntityId(), this);
        }

        public void OnDisable()
        {
            Systems.CustomId.DisableComponent(gameObject.GetEntityId());
        }

        public IComponentSystem GetRelatedSystem()
        {
            return Systems.CustomId;
        }
    }
}
