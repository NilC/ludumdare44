﻿using UnityEngine;

namespace LD44
{
    public class CustomIdSystem : ComponentSystem<CustomIdComponent>
    {
        public int GetEntityIdByCustomId(int customId)
        {
            foreach (var componentPair in Components)
            {
                var component = componentPair.Value;
                if (component.CustomId == customId)
                {
                    return component.EntityId;
                }
            }

            throw new UnityException(string.Format("No entity with custom id: {0}", customId));
        }
    }

    public partial class Systems
    {
        public static CustomIdSystem CustomId = new CustomIdSystem();
    }
}
