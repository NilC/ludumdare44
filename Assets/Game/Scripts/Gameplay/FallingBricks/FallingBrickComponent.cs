﻿using UnityEngine;

namespace LD44
{
    public class FallingBrickComponent : MonoBehaviour, IComponent
    {
        public int DebrisPrototypeId;

        public int EntityId { private set; get; }

        public void Awake()
        {
            EntityId = gameObject.GetEntityId();
        }

        public void OnEnable()
        {
            Systems.FallingBricks.AddComponent(gameObject.GetEntityId(), this);
        }

        public void OnDisable()
        {
            Systems.FallingBricks.DisableComponent(gameObject.GetEntityId());
        }

        public void OnCollisionEnter2D(Collision2D collision)
        {
            Systems.FallingBricks.ProcessCollisionEnter(this, collision);
        }

        public IComponentSystem GetRelatedSystem()
        {
            return Systems.FallingBricks;
        }
    }
}