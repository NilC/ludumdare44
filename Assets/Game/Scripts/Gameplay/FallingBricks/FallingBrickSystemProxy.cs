﻿using UnityEngine;

namespace LD44
{
    public class FallingBrickSystemProxy : MonoBehaviour
    {
        public float SpawnFrequency;
        public Transform BrickSpawnPoint;

        private void Awake()
        {
            Systems.FallingBricks.SpawnFrequency = SpawnFrequency;
            Systems.FallingBricks.BrickSpawnPoint = BrickSpawnPoint;
        }
    }
}