﻿using UnityEngine;

namespace LD44
{
    public class FallingBrickSystem : ComponentSystem<FallingBrickComponent>
    {
        public float SpawnFrequency;
        public Transform BrickSpawnPoint;

        private float currentSpawnTime = 0.0f;

        public override void Initialize()
        {
            base.Initialize();

            currentSpawnTime = SpawnFrequency - SpawnFrequency * 0.4f;
        }

        public override void Run()
        {
            currentSpawnTime += Time.deltaTime;
            if (currentSpawnTime > SpawnFrequency)
            {
                Systems.Entities.AddCreateRequest(new CreateRequest
                {
                    PrototypeId = PrototypeId.Brick,
                    Position = BrickSpawnPoint.position
                });
                currentSpawnTime = 0.0f;
            }
        }

        public override void ResetComponent(int entityId)
        {
            var component = Components[entityId];
            component.transform.localRotation = Quaternion.identity;
        }

        public void ProcessCollisionEnter(FallingBrickComponent fallingBrickComponent, Collision2D collision)
        {
            var heroEntityId = Systems.CustomId.GetEntityIdByCustomId(CustomId.Hero);

            bool isHero = false;
            Vector2 averagePoint = new Vector2();
            for (var i = 0; i < collision.contacts.Length; i++)
            {
                if (collision.contacts[i].collider.gameObject.GetEntityId() == heroEntityId ||
                    collision.contacts[i].otherCollider.gameObject.GetEntityId() == heroEntityId)
                {
                    isHero = true;
                }

                averagePoint += collision.contacts[i].point;
            }
            averagePoint /= collision.contacts.Length;

            Systems.Entities.AddCreateRequest(new CreateRequest
            {
                PrototypeId = fallingBrickComponent.DebrisPrototypeId,
                Position = averagePoint
            });
            Systems.Entities.AddDestroyRequest(fallingBrickComponent.EntityId);

            if (isHero && !GameVariables.Instance.MustWearItemTypes.Contains(MustWearItemType.Helmet))
            {
                GameVariables.Instance.LastTraumaType = TraumaType.Head;
                Systems.Crippled.ProcessTrauma();
            }
        }
    }

    public partial class Systems
    {
        public static FallingBrickSystem FallingBricks = new FallingBrickSystem();
    }
}