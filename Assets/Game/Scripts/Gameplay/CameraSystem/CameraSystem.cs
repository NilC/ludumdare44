﻿namespace LD44
{
    using UnityEngine;

    public class CameraSystem : ComponentSystem<CameraComponent>
    {
        public override void Initialize()
        {
            base.Initialize();

            foreach (var componentPair in Components)
            {
                var component = componentPair.Value;
                if (component.CustomIdToFollow != 0)
                {
                    component.EntityIdToFollow = Systems.CustomId.GetEntityIdByCustomId(component.CustomIdToFollow);
                }
            }
        }

        public override void FixedRun()
        {
            foreach (var componentPair in Components)
            {
                UpdateFollowing(componentPair.Value);
            }
        }

        private void UpdateFollowing(CameraComponent component)
        {
            var entityIdToFollow = component.EntityIdToFollow;
            if (entityIdToFollow != 0)
            {
                var cameraPosition = component.transform.position;
                var targetPosition = Systems.Entities.GetComponent(entityIdToFollow).transform.position;
                targetPosition += component.Offset;

                Vector3 delta = Vector3.zero;

                float dx = targetPosition.x - cameraPosition.x;

                if (dx > component.BoundX || dx < -component.BoundX)
                {
                    if (cameraPosition.x < targetPosition.x)
                    {
                        delta.x = dx - component.BoundX;
                    }
                    else
                    {
                        delta.x = dx + component.BoundX;
                    }
                }

                float dy = targetPosition.y - cameraPosition.y;

                if (dy > component.BoundY || dy < -component.BoundY)
                {
                    if (cameraPosition.y < targetPosition.y)
                    {
                        delta.y = dy - component.BoundY;
                    }
                    else
                    {
                        delta.y = dy + component.BoundY;
                    }
                }

                Vector3 newCameraPosition;
                if (component.IsLerp)
                {
                    Vector3 desiredPosition = cameraPosition + delta;
                    float x = Mathf.Lerp(cameraPosition.x, desiredPosition.x, component.SmoothTime.x);
                    float y = Mathf.Lerp(cameraPosition.y, desiredPosition.y, component.SmoothTime.y);
                    newCameraPosition = new Vector3(x, y, cameraPosition.z);
                }
                else
                {
                    newCameraPosition = component.transform.position + delta;
                }

                component.transform.position = new Vector3(
                    Mathf.Clamp(newCameraPosition.x, component.MinX, component.MaxX),
                    Mathf.Clamp(newCameraPosition.y, component.MinY, component.MaxY),
                    newCameraPosition.z);
            }
        }
    }

    public partial class Systems
    {
        public static CameraSystem Cameras = new CameraSystem();
    }
}