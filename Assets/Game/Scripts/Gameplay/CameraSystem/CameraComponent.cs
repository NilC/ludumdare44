﻿using UnityEngine;

namespace LD44
{
    public class CameraComponent : MonoBehaviour, IComponent
    {
        public float BoundX;
        public float BoundY;
        public Vector3 Offset;

        public bool IsLerp;
        public Vector2 SmoothTime;

        public float MinX;
        public float MaxX;
        public float MinY;
        public float MaxY;

        public int CustomIdToFollow;
        [HideInInspector] public int EntityIdToFollow;

        [HideInInspector] public Vector3 Velocity;
        [HideInInspector] public Camera Camera;

        public int EntityId { private set; get; }

        public void Awake()
        {
            EntityId = gameObject.GetEntityId();
        }

        public void OnEnable()
        {
            Camera = GetComponent<Camera>();
            Systems.Cameras.AddComponent(gameObject.GetEntityId(), this);
        }

        public void OnDisable()
        {
            Systems.Cameras.DisableComponent(gameObject.GetEntityId());
        }

        public IComponentSystem GetRelatedSystem()
        {
            return Systems.Cameras;
        }
    }
}