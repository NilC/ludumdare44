using UnityEngine;

namespace LD44
{
    public class HeroComponent : MonoBehaviour, IComponent
    {
        public float MaxSpeed;
        public float InetriaFactor;
        public float JumpTakeOffSpeed;
        public float JumpSlowDownFactor;
        public float GroudDistance;

        public bool IsFacingRight;
        public float ScaleX;

        public Transform BodyBleedingPoint;

        [HideInInspector] public PolygonCollider2D PolygonCollider2D;

        public int EntityId { private set; get; }

        public void Awake()
        {
            PolygonCollider2D = GetComponent<PolygonCollider2D>();
            EntityId = gameObject.GetEntityId();
        }

        public void OnEnable()
        {
            Systems.Heroes.AddComponent(gameObject.GetEntityId(), this);
        }

        public void OnDisable()
        {
            Systems.Heroes.DisableComponent(gameObject.GetEntityId());
        }

        public IComponentSystem GetRelatedSystem()
        {
            return Systems.Heroes;
        }
    }
}