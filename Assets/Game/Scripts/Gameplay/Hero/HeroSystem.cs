using UnityEngine;

namespace LD44
{
    public class HeroSystem : ComponentSystem<HeroComponent>
    {
        public const string GroundMaskName = "Environment";

        private Camera mainCamera;

        private int groundMask;
        private RaycastHit2D[] groundHitResults = new RaycastHit2D[10];

        public override void Initialize()
        {
            mainCamera = Camera.main;

            var windowAspect = Screen.width / (float)Screen.height;
            var scaleHeight = windowAspect / 1.76f;

            if (scaleHeight < 1.0f)
            {
                mainCamera.orthographicSize = mainCamera.orthographicSize / scaleHeight;
            }

            groundMask = LayerMask.GetMask(GroundMaskName);
        }

        public override void Run()
        {
            ProcessFlip();
        }

        public void Jump(int entityId, JumpState jumpState)
        {
            var component = Components[entityId];

            if (jumpState == JumpState.Hit)
            {
                var isGrounded = IsGrounded(entityId, component);
                if (isGrounded)
                {
                    AddRigitbodyVelocityYRequest(entityId, component.JumpTakeOffSpeed);
                }
            }
            else if (jumpState == JumpState.Released)
            {
                var currentY = Systems.RigitbodyPositioning.GetComponent(entityId).Rigidbody2D.velocity.y;
                AddRigitbodyVelocityYRequest(entityId, currentY * component.JumpSlowDownFactor);
            }
        }

        public void HorizontalMove(int entityId, float horizontalMove)
        {
            var component = Components[entityId];

            float velocityX = component.MaxSpeed * horizontalMove;

            if (!Mathf.Approximately(horizontalMove, 0.0f))
            {
                AddRigitbodyVelocityXRequest(entityId, velocityX);
            }
            else
            {
                var currentX = Systems.RigitbodyPositioning.GetComponent(entityId).Rigidbody2D.velocity.x;
                if (!MathHelper.Approximately(currentX, 0.0f, 0.01f))
                {
                    AddRigitbodyVelocityXRequest(entityId, currentX * component.InetriaFactor);
                }
            }
        }

        private bool IsGrounded(int entityId, HeroComponent component)
        {
            var position = component.transform.position;

            var direction = new Vector2(0.0f, -1.0f);
            var groudDistance = component.GroudDistance;

            var leftPosition = new Vector2(position.x - component.PolygonCollider2D.bounds.extents.x, position.y);
            var leftRayResultsCount = Physics2D.RaycastNonAlloc(leftPosition, direction, groundHitResults, groudDistance, groundMask);

            var centerPosition = new Vector2(position.x, position.y);
            var centerRayResultsCount = Physics2D.RaycastNonAlloc(centerPosition, direction, groundHitResults, groudDistance, groundMask);

            var rightPosition = new Vector2(position.x + component.PolygonCollider2D.bounds.extents.x, position.y);
            var rightRayResultsCount = Physics2D.RaycastNonAlloc(rightPosition, direction, groundHitResults, groudDistance, groundMask);

            return leftRayResultsCount > 0 || centerRayResultsCount > 0 || rightRayResultsCount > 0;
        }


        private void ProcessFlip()
        {
            foreach (var componentPair in Components)
            {
                var component = componentPair.Value;
                var transform = component.transform;
                var velocityX = Systems.RigitbodyPositioning.GetComponent(componentPair.Key).Rigidbody2D.velocity.x;

                if (component.IsFacingRight && velocityX < -0.1f)
                {
                    component.IsFacingRight = false;
                    transform.localScale = new Vector3(-component.ScaleX, transform.localScale.y, transform.localScale.z);
                }

                if (!component.IsFacingRight && velocityX > 0.1f)
                {
                    component.IsFacingRight = true;
                    transform.localScale = new Vector3(component.ScaleX, transform.localScale.y, transform.localScale.z);
                }
            }
        }

        private void AddRigitbodyVelocityXRequest(int entityId, float velocityX)
        {
            Systems.RigitbodyPositioning.AddVelocityXRequest(new VelocityXRequest
            {
                EntityId = entityId,
                VelocityX = velocityX
            });
        }

        private void AddRigitbodyVelocityYRequest(int entityId, float velocityY)
        {
            Systems.RigitbodyPositioning.AddVelocityYRequest(new VelocityYRequest
            {
                EntityId = entityId,
                VelocityY = velocityY
            });
        }
    }

    public partial class Systems
    {
        public static HeroSystem Heroes = new HeroSystem();
    }
}
