﻿using UnityEngine;

namespace LD44
{
    public class InputComponent : MonoBehaviour, IComponent
    {
        public int EntityId { private set; get; }

        public void Awake()
        {
            EntityId = gameObject.GetEntityId();
        }

        public void OnEnable()
        {
            Systems.Input.AddComponent(gameObject.GetEntityId(), this);
        }

        public void OnDisable()
        {
            Systems.Input.DisableComponent(gameObject.GetEntityId());
        }

        public IComponentSystem GetRelatedSystem()
        {
            return Systems.Input;
        }
    }
}