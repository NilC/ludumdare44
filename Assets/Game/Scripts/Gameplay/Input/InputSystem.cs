﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace LD44
{
    public class InputSystem : ComponentSystem<InputComponent>
    {
        private Camera mainCamera;

        public override void Initialize()
        {
            mainCamera = Camera.main;
        }

        public override void Run()
        {
            foreach (var componentPair in Components)
            {
                UpdateMovement(componentPair.Key);
                UpdateJumping(componentPair.Key);
            }
        }

        private void UpdateMovement(int entityId)
        {
            float horizontalMove = Input.GetAxis("Horizontal");
            Systems.Heroes.HorizontalMove(entityId, horizontalMove);
        }

        private void UpdateJumping(int entityId)
        {
            if (Input.GetButtonDown("Jump"))
            {
                Systems.Heroes.Jump(entityId, JumpState.Hit);
            }
            else if (Input.GetButtonUp("Jump"))
            {
                Systems.Heroes.Jump(entityId, JumpState.Released);
            }
        }

#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
        public static bool IsTapped()
        {
            return Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Began && !IsPointerOverUI();
        }

        public static bool IsStartedMoving()
        {
            return Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Began;
        }

        public static bool IsMoving()
        {
            return Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Moved;
        }

        public Vector3 GetInputPosition()
        {
            Vector3 mousePos = new Vector3(Input.GetTouch(0).position.x, Input.GetTouch(0).position.y, -mainCamera.transform.position.z);
            return mainCamera.ScreenToWorldPoint(mousePos);
        }

        public static Vector3 GetInputScreenPosition()
        {
            return Input.GetTouch(0).position;
        }
#else
        public static bool IsStartedMoving()
        {
            return Input.GetMouseButtonDown(0);
        }

        public static bool IsMoving()
        {
            return Input.GetMouseButton(0);
        }

        public static bool IsTapped()
        {
            return Input.GetMouseButtonUp(0) && !IsPointerOverUI();
        }

        public Vector3 GetInputPosition()
        {
            var mousePos = Input.mousePosition;
            mousePos.z = -mainCamera.transform.position.z; // select distance = 10 units from the camera
            return mainCamera.ScreenToWorldPoint(mousePos);
            //return Dependences.MainCamera.ScreenToWorldPoint(Input.mousePosition);
        }

        public static Vector3 GetInputScreenPosition()
        {
            return Input.mousePosition;
        }
#endif

        private static bool IsPointerOverUI()
        {
            if (!EventSystem.current)
            {
                return false;
            }

            return EventSystem.current.currentSelectedGameObject;
            //return EventSystem.current.IsPointerOverGameObject();
        }
    }

    public partial class Systems
    {
        public static InputSystem Input = new InputSystem();
    }
}