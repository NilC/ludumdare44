﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace LD44
{
    public class CrippledSystem : SimpleSystem
    {
        public void ProcessTrauma()
        {
            CoroutineHelper.Instance.PlayCoroutine(ProcessTraumaCourutine());
        }

        private IEnumerator ProcessTraumaCourutine()
        {
            var heroEntityId = Systems.CustomId.GetEntityIdByCustomId(CustomId.Hero);
            Vector3 heroPosition = Systems.Heroes.GetComponent(heroEntityId).transform.position;
            Vector3 offscreenPosition = heroPosition - new Vector3(16.0f, 0.0f, 0.0f);

            Systems.Input.IsNeedToRun = false;
            Systems.FallingBricks.IsNeedToRun = false;
            Systems.Cameras.IsNeedToRun = false;

            yield return new WaitForSeconds(0.2f);

            Systems.Entities.AddCreateRequest(new CreateRequest
            {
                PrototypeId = PrototypeId.Ambulance,
                Position = offscreenPosition
            });

            Systems.Entities.AddCreateRequest(new CreateRequest
            {
                PrototypeId = PrototypeId.TraumaFadeCurtain,
                Position = heroPosition
            });

            yield return null; //Skip one frame

            var traumaFadeCurtainEntityId = Systems.CustomId.GetEntityIdByCustomId(CustomId.TraumaFadeCurtain);

            var traumaFadeCurtainSpriteRenderer = Systems.Entities.GetComponent(traumaFadeCurtainEntityId).GetComponent<SpriteRenderer>();
            while (traumaFadeCurtainSpriteRenderer.color.a < 0.9999f)
            {
                traumaFadeCurtainSpriteRenderer.color = new Color(
                    traumaFadeCurtainSpriteRenderer.color.r,
                    traumaFadeCurtainSpriteRenderer.color.b,
                    traumaFadeCurtainSpriteRenderer.color.b,
                    traumaFadeCurtainSpriteRenderer.color.a + Time.deltaTime * 3.0f);
                yield return null;
            }

            var ambulanceEntityId = Systems.CustomId.GetEntityIdByCustomId(CustomId.Ambulance);
            Systems.MovementToPoint.UpdatePoint(ambulanceEntityId, heroPosition);

            var ambulanceTransform = Systems.Entities.GetComponent(ambulanceEntityId).transform;

            while (!Systems.MovementToPoint.GetComponent(ambulanceEntityId).IsPointReached)
            {
                var currentHeroPosition = Systems.Heroes.GetComponent(heroEntityId).transform.position;
                var updatedPoint = new Vector3(ambulanceTransform.position.x, currentHeroPosition.y, ambulanceTransform.position.z);
                ambulanceTransform.position = updatedPoint;
                Systems.MovementToPoint.UpdatePoint(ambulanceEntityId, currentHeroPosition);
                yield return null;
            }

            yield return new WaitForSeconds(0.1f);

            Systems.Entities.AddDestroyRequest(heroEntityId);

            yield return new WaitForSeconds(0.5f);

            ambulanceTransform.localScale = new Vector3(-ambulanceTransform.localScale.x, ambulanceTransform.localScale.y, ambulanceTransform.localScale.z);
            offscreenPosition = new Vector3(offscreenPosition.x, ambulanceTransform.position.y, offscreenPosition.z);
            Systems.MovementToPoint.UpdatePoint(ambulanceEntityId, offscreenPosition);

            while (!Systems.MovementToPoint.GetComponent(ambulanceEntityId).IsPointReached)
            {
                yield return null;
            }

            GameController.Instance.GoToScene(SceneName.Insurance);
        }
    }

    public partial class Systems
    {
        public static CrippledSystem Crippled = new CrippledSystem();
    }
}