﻿namespace LD44
{
    public static class PrototypeId
    {
        public const int Hero = 1;
        public const int Brick = 1000;
        public const int BloodDebris = 2000;
        public const int Ambulance = 3000;
        public const int TraumaFadeCurtain = 3001;
    }
}

