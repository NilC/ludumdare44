﻿using UnityEngine;

namespace LD44
{
    public class LinearMovementComponent : MonoBehaviour, IComponent
    {
        public float Speed;
        public float InitialSpeed;
        public float SpeedModificator;
        public float InitialSpeedModificator;
        public Vector3 DirectionVector;

        public int EntityId { private set; get; }

        public void Awake()
        {
            EntityId = gameObject.GetEntityId();
        }

        public void OnEnable()
        {
            Systems.LinearMovement.AddComponent(gameObject.GetEntityId(), this);
        }

        public void OnDisable()
        {
            Systems.LinearMovement.DisableComponent(gameObject.GetEntityId());
        }

        public IComponentSystem GetRelatedSystem()
        {
            return Systems.LinearMovement;
        }
    }
}