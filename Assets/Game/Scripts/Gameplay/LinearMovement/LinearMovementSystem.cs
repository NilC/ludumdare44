﻿using UnityEngine;

namespace LD44
{
    public class LinearMovementSystem : ComponentSystem<LinearMovementComponent>
    {
        public override void Run()
        {
            foreach (var componentPair in Components)
            {
                var component = componentPair.Value;
                if (component.enabled)
                {
                    component.transform.position += component.DirectionVector * GetDeltaSpeed(component);
                }
            }
        }

        public void UpdateDirection(int entityId, Vector3 direction)
        {
            Components[entityId].DirectionVector = direction;
            Components[entityId].DirectionVector.Normalize();
        }

        private float GetDeltaSpeed(LinearMovementComponent component)
        {
            return component.Speed * component.SpeedModificator * Time.deltaTime;
        }
    }

    public partial class Systems
    {
        public static LinearMovementSystem LinearMovement = new LinearMovementSystem();
    }
}