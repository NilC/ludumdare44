﻿using UnityEngine;

namespace LD44
{
    public class LifetimeSystem : ComponentSystem<LifetimeComponent>
    {
        public override void Run()
        {
            foreach (var componentPair in Components)
            {
                var component = componentPair.Value;
                component.CurrentLifetime += Time.deltaTime;
                if (component.CurrentLifetime > component.Lifetime)
                {
                    Systems.Entities.AddDestroyRequest(component.EntityId);
                }
            }
        }

        public override void ResetComponent(int entityId)
        {
            var component = Components[entityId];
            component.CurrentLifetime = 0.0f;
        }
    }

    public partial class Systems
    {
        public static LifetimeSystem Lifetime = new LifetimeSystem();
    }
}
