﻿using UnityEngine;

namespace LD44
{
    public class LifetimeComponent : MonoBehaviour, IComponent
    {
        public float Lifetime;
        [HideInInspector] public float CurrentLifetime;

        public int EntityId { private set; get; }

        public void Awake()
        {
            EntityId = gameObject.GetEntityId();
        }

        public void OnEnable()
        {
            Systems.Lifetime.AddComponent(gameObject.GetEntityId(), this);
        }

        public void OnDisable()
        {
            Systems.Lifetime.DisableComponent(gameObject.GetEntityId());
        }

        public IComponentSystem GetRelatedSystem()
        {
            return Systems.Lifetime;
        }
    }
}
