﻿using UnityEngine;

namespace LD44
{
    public class SpikesSystem : ComponentSystem<SpikesComponent>
    {
        public float SpawnFrequency;
        public Transform BrickSpawnPoint;

        private float currentSpawnTime = 0.0f;

        public override void Initialize()
        {
            base.Initialize();

            currentSpawnTime = SpawnFrequency - SpawnFrequency * 0.4f;
        }

        public override void Run()
        {
            currentSpawnTime += Time.deltaTime;
            if (currentSpawnTime > SpawnFrequency)
            {
                Systems.Entities.AddCreateRequest(new CreateRequest
                {
                    PrototypeId = PrototypeId.Brick,
                    Position = BrickSpawnPoint.position
                });
                currentSpawnTime = 0.0f;
            }
        }

        public override void ResetComponent(int entityId)
        {
            var component = Components[entityId];
            component.transform.localRotation = Quaternion.identity;
        }

        public void ProcessTriggerEnter(SpikesComponent fallingBrickComponent, Collider2D collider)
        {
            var heroEntityId = Systems.CustomId.GetEntityIdByCustomId(CustomId.Hero);
            bool isHero = collider.gameObject.GetEntityId() == heroEntityId;
            if (isHero && !GameVariables.Instance.MustWearItemTypes.Contains(MustWearItemType.Pillow))
            {
                Systems.Entities.AddCreateRequest(new CreateRequest
                {
                    PrototypeId = PrototypeId.BloodDebris,
                    Position = Systems.Heroes.GetComponent(heroEntityId).BodyBleedingPoint.position
                });

                Systems.RigitbodyPositioning.AddVelocityXRequest(new VelocityXRequest
                {
                    EntityId = heroEntityId,
                    VelocityX = 5.0f
                });

                GameVariables.Instance.LastTraumaType = TraumaType.BodyPiercing;
                Systems.Crippled.ProcessTrauma();
            }
        }
    }

    public partial class Systems
    {
        public static SpikesSystem Spikes = new SpikesSystem();
    }
}