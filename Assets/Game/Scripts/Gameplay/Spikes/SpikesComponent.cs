﻿using UnityEngine;

namespace LD44
{
    public class SpikesComponent : MonoBehaviour, IComponent
    {
        public int EntityId { private set; get; }

        public void Awake()
        {
            EntityId = gameObject.GetEntityId();
        }

        public void OnEnable()
        {
            Systems.Spikes.AddComponent(gameObject.GetEntityId(), this);
        }

        public void OnDisable()
        {
            Systems.Spikes.DisableComponent(gameObject.GetEntityId());
        }

        public void OnTriggerEnter2D(Collider2D collision)
        {
            Systems.Spikes.ProcessTriggerEnter(this, collision);
        }

        public IComponentSystem GetRelatedSystem()
        {
            return Systems.Spikes;
        }
    }
}