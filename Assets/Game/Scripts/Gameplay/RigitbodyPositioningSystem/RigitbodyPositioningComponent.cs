﻿using UnityEngine;

namespace LD44
{
    public class RigitbodyPositioningComponent : MonoBehaviour, IComponent
    {
        [HideInInspector] public Rigidbody2D Rigidbody2D;

        public int EntityId { private set; get; }

        public void Awake()
        {
            EntityId = gameObject.GetEntityId();
            Rigidbody2D = GetComponent<Rigidbody2D>();
        }

        public void OnEnable()
        {
            Systems.RigitbodyPositioning.AddComponent(gameObject.GetEntityId(), this);
        }

        public void OnDisable()
        {
            Systems.RigitbodyPositioning.DisableComponent(gameObject.GetEntityId());
        }

        public IComponentSystem GetRelatedSystem()
        {
            return Systems.RigitbodyPositioning;
        }
    }
}