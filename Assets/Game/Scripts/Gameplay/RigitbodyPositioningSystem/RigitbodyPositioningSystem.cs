﻿using System.Collections.Generic;
using UnityEngine;

namespace LD44
{
    public struct VelocityXRequest
    {
        public int EntityId;
        public float VelocityX;
    }

    public struct VelocityYRequest
    {
        public int EntityId;
        public float VelocityY;
    }

    public class RigitbodyPositioningSystem : ComponentSystem<RigitbodyPositioningComponent>
    {
        private Dictionary<int, Vector3> updatedComponents = new Dictionary<int, Vector3>(100);

        private List<VelocityXRequest> velocityXRequests = null;
        private List<VelocityYRequest> velocityYRequests = null;

        public override void Initialize()
        {
            base.Initialize();

            WarmupRequestsContainers();
        }

        public override void FixedRun()
        {
            for (int i = 0; i < velocityXRequests.Count; i++)
            {
                var velocityXRequest = velocityXRequests[i];
                var component = Components[velocityXRequest.EntityId];
                component.Rigidbody2D.velocity = new Vector2(velocityXRequest.VelocityX, component.Rigidbody2D.velocity.y);
            }
            velocityXRequests.Clear();

            for (int i = 0; i < velocityYRequests.Count; i++)
            {
                var velocityYRequest = velocityYRequests[i];
                var component = Components[velocityYRequest.EntityId];
                component.Rigidbody2D.velocity = new Vector2(component.Rigidbody2D.velocity.x, velocityYRequest.VelocityY);
            }
            velocityYRequests.Clear();
        }

        public void AddVelocityXRequest(VelocityXRequest velocityXRequest)
        {
            velocityXRequests.Add(velocityXRequest);
        }

        public void AddVelocityYRequest(VelocityYRequest velocityYRequest)
        {
            velocityYRequests.Add(velocityYRequest);
        }

        private void WarmupRequestsContainers()
        {
            velocityXRequests = new List<VelocityXRequest>(30);
            velocityYRequests = new List<VelocityYRequest>(30);
        }
    }

    public partial class Systems
    {
        public static RigitbodyPositioningSystem RigitbodyPositioning = new RigitbodyPositioningSystem();
    }
}
