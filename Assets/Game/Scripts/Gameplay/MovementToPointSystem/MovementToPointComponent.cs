﻿using UnityEngine;

namespace LD44
{
    public class MovementToPointComponent : MonoBehaviour, IComponent
    {
        public float Accuracy;
        public float Speed;
        public float SpeedModificator;
        public Vector3 Point;
        public bool IsPointReached;

        [HideInInspector] public float SqrAccuracy;

        public int EntityId { private set; get; }

        public void Awake()
        {
            SqrAccuracy = Accuracy * Accuracy;
            EntityId = gameObject.GetEntityId();
        }

        public void OnEnable()
        {
            Systems.MovementToPoint.AddComponent(gameObject.GetEntityId(), this);
        }

        public void OnDisable()
        {
            Systems.MovementToPoint.DisableComponent(gameObject.GetEntityId());
        }

        public IComponentSystem GetRelatedSystem()
        {
            return Systems.MovementToPoint;
        }
    }
}
