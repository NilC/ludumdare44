﻿using UnityEngine;

namespace LD44
{
    public class MovementToPointSystem : ComponentSystem<MovementToPointComponent>
    {
        public void UpdatePoint(int entityId, Vector3 point)
        {
            var component = Components[entityId];
            component.Point = point;
            component.IsPointReached = false;
        }

        public override void Run()
        {
            ProcessMovement();
        }

        private void ProcessMovement()
        {
            foreach (var componentPair in Components)
            {
                var component = componentPair.Value;

                var currentPositionPoint = component.transform.position;

                var offset = component.Point - currentPositionPoint;
                var isPointReached = offset.sqrMagnitude < component.SqrAccuracy;

                if (isPointReached)
                {
                    component.IsPointReached = true;
                    continue;
                }

                var directionVector = component.Point - currentPositionPoint;
                directionVector.Normalize();

                var nextPosition = currentPositionPoint + directionVector * GetDeltaSpeed(component);

                //учитывается "перелет" позиции из-за больших скоростей, или большого RealtimeDeltaTime
                if ((currentPositionPoint.x <= component.Point.x && component.Point.x <= nextPosition.x ||
                     nextPosition.x <= component.Point.x && component.Point.x <= currentPositionPoint.x) &&

                    (currentPositionPoint.y <= component.Point.y && component.Point.y <= nextPosition.y ||
                     nextPosition.y <= component.Point.y && component.Point.y <= currentPositionPoint.y) &&

                    (currentPositionPoint.z <= component.Point.z && component.Point.z <= nextPosition.z ||
                     nextPosition.z <= component.Point.z && component.Point.z <= currentPositionPoint.z))
                {
                    nextPosition = component.Point;
                }

                component.transform.position += nextPosition - currentPositionPoint;
            }
        }

        private float GetDeltaSpeed(MovementToPointComponent component)
        {
            return component.Speed * component.SpeedModificator * Time.deltaTime;
        }
    }

    public partial class Systems
    {
        public static MovementToPointSystem MovementToPoint = new MovementToPointSystem();
    }
}
