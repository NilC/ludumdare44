﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;

namespace LD44.Editor
{
    [CustomEditor(typeof(PrototypeLoockup), true)]
    public class PrototypeLoockupEditor : CustomInspectorEditor
    {
        private const float ButtonWidth = 100;

        private const string Groups = "groups";

        private const string Name = "Name";
        private const string Pairs = "Pairs";
        private const string PrototypeId = "PrototypeId";
        private const string Prototype = "Prototype";

        private TableView[] groupTableViews;
        string newGroupName;
        PrototypeLoockupEditorState state;

        private void OnEnable()
        {
            state = PrototypeLoockupEditorState.Editing;

            var groupsProperty = serializedObject.FindProperty(Groups);

            groupTableViews = new TableView[groupsProperty.arraySize];

            for (int i = 0; i < groupTableViews.Length; i++)
            {
                var groupProperty = groupsProperty.GetArrayElementAtIndex(i);
                var groupName = groupProperty.FindPropertyRelative(Name).stringValue;
                var pairsProperty = groupProperty.FindPropertyRelative(Pairs);
                groupTableViews[i] = CreateGroupTableView(i, groupName, pairsProperty);
            }

            UpdatePrototypesIds();
        }

        private void OnDisable()
        {
            UpdatePrototypesIds();
        }

        private void OnLostFocus()
        {
            UpdatePrototypesIds();
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            DrawScriptFieldFromScriptableObject();

            DrawPrototypeGroups();

            GUILayout.Space(2.0f);
            GUILayout.BeginHorizontal();
            {
                GUILayout.FlexibleSpace();
                if (GUILayout.Button("Force update", GUILayout.ExpandWidth(true), GUILayout.Width(250)))
                {
                    UpdatePrototypesIds();
                }
                GUILayout.FlexibleSpace();
            }
            GUILayout.EndHorizontal();
        }

        protected void DrawPrototypeGroups()
        {
            serializedObject.Update();

            var groupsProperty = serializedObject.FindProperty(Groups);

            for (var i = 0; i < groupsProperty.arraySize; i++)
            {
                groupTableViews[i].Draw();
            }

            DrawAddGroup(groupsProperty);

            serializedObject.ApplyModifiedProperties();
        }

        private void DrawAddGroup(SerializedProperty groupsProperty)
        {
            GUILayout.Space(2.5f);

            if (state == PrototypeLoockupEditorState.AddingNewGroup)
            {
                GUILayout.Space(1.5f);
                GUILayout.BeginHorizontal();
                {
                    newGroupName = EditorGUILayout.TextField("New Group Name: ", newGroupName);
                }
                GUILayout.EndHorizontal();
            }

            GUILayout.Space(1.5f);
            GUILayout.BeginHorizontal();
            {
                GUILayout.FlexibleSpace();
                if (GUILayout.Button("Add Group", GUILayout.ExpandWidth(true), GUILayout.Width(ButtonWidth)))
                {
                    if (state == PrototypeLoockupEditorState.AddingNewGroup)
                    {
                        groupsProperty.InsertArrayElementAtIndex(groupsProperty.arraySize);
                        var lastGroupProperty = groupsProperty.GetArrayElementAtIndex(groupsProperty.arraySize - 1);
                        lastGroupProperty.FindPropertyRelative(Name).stringValue = newGroupName;

                        var pairsProperty = lastGroupProperty.FindPropertyRelative(Pairs);
                        pairsProperty.ClearArray();

                        var newGroupTableView = CreateGroupTableView(groupsProperty.arraySize - 1, newGroupName, pairsProperty);
                        ArrayUtility.Add(ref groupTableViews, newGroupTableView);
                    }

                    if (state == PrototypeLoockupEditorState.AddingNewGroup)
                    {
                        state = PrototypeLoockupEditorState.Editing;
                    }
                    else if (state == PrototypeLoockupEditorState.Editing)
                    {
                        state = PrototypeLoockupEditorState.AddingNewGroup;
                    }
                }

                if (state == PrototypeLoockupEditorState.AddingNewGroup)
                {
                    if (GUILayout.Button("Cancel", GUILayout.ExpandWidth(true), GUILayout.Width(ButtonWidth)))
                    {
                        state = PrototypeLoockupEditorState.Editing;
                    }
                }

                GUILayout.FlexibleSpace();
            }
            GUILayout.EndHorizontal();
        }

        private void UpdatePrototypesIds()
        {
            var groupsProperty = serializedObject.FindProperty(Groups);
            for (var i = 0; i < groupsProperty.arraySize; i++)
            {
                var pairsProperty = groupsProperty.GetArrayElementAtIndex(i).FindPropertyRelative(Pairs);

                for (int j = 0; j < pairsProperty.arraySize; j++)
                {
                    var prototype = pairsProperty.GetArrayElementAtIndex(j).FindPropertyRelative(Prototype).objectReferenceValue;
                    var prototypeId = pairsProperty.GetArrayElementAtIndex(j).FindPropertyRelative(PrototypeId).intValue;
                    ResetPrototypeId(prototype as GameObject, prototypeId);
                }
            }
            SaveAndRefreshAssets();
        }

        private TableView CreateGroupTableView(int index, string groupName, SerializedProperty pairsProperty)
        {
            System.Action<SerializedProperty> afterAddedCallback = (SerializedProperty property) =>
            {
                var id = index == 0 ? 1 : index * 1000;

                if (pairsProperty.arraySize > 1)
                {
                    for (int i = 0; i < pairsProperty.arraySize; i++)
                    {
                        int currentId = pairsProperty.GetArrayElementAtIndex(i).FindPropertyRelative(PrototypeId).intValue;
                        if (currentId >= id)
                        {
                            id = currentId + 1;
                        }
                    }
                }

                property.FindPropertyRelative(PrototypeId).intValue = id;
                property.FindPropertyRelative(Prototype).objectReferenceValue = null;
            };

            System.Action afterTableRemoveCallback = () =>
            {
                var groupsProperty = serializedObject.FindProperty(Groups);
                groupsProperty.DeleteArrayElementAtIndex(index);
                ArrayUtility.RemoveAt(ref groupTableViews, index);
            };

            return new TableView(serializedObject, pairsProperty, groupName,
                new TableColumn[]
                {
                    new TableColumn
                    {
                        Header = PrototypeId.ToCamelCaseWithSpaces(),
                        WidthFactor = 0.5f,
                        FieldName = PrototypeId
                    },
                    new TableColumn
                    {
                        Header = Prototype.ToCamelCaseWithSpaces(),
                        WidthFactor = 0.5f,
                        FieldName = Prototype
                    }
                },
                TableFoldoutMode.Foldout,
                TableRowDragableMode.Dragable,
                TableRemoveMode.WithRemove,
                afterAddedCallback,
                afterTableRemoveCallback);
        }

        private void ResetPrototypeId(GameObject gameObject, int prototypeId)
        {
            if (gameObject != null)
            {
                EntityComponent entityComponent = gameObject.GetComponent<EntityComponent>();
                if (entityComponent != null)
                {
                    entityComponent.PrototypeId = prototypeId;
                    EditorUtility.SetDirty(entityComponent);
                }
                else
                {
                    Debug.LogWarning(string.Format("Entity has no component constructor. Entity name: {0}", gameObject.name));
                }
            }
        }
    }
}

#endif