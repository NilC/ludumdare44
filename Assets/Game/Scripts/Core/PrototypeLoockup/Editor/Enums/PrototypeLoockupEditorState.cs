﻿namespace LD44.Editor
{
    public enum PrototypeLoockupEditorState
    {
        None,
        Editing,
        AddingNewGroup
    }
}