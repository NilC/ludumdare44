﻿using System;
using Object = UnityEngine.Object;

namespace LD44
{
    [Serializable]
    public struct PrototypePair
    {
        public int PrototypeId;
        public Object Prototype;

        public PrototypePair(int prototypeId, Object prototype)
        {
            PrototypeId = prototypeId;
            Prototype = prototype;
        }
    }
}