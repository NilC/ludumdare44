﻿using System;

namespace LD44
{
    [Serializable]
    public struct PrototypeGroup
    {
        public PrototypeGroup(string name)
        {
            Name = name;
            Pairs = new PrototypePair[0];
        }

        public string Name;
        public PrototypePair[] Pairs;
    }
}