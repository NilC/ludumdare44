﻿using System.Collections.Generic;
using UnityEngine;

namespace LD44
{
    [CreateAssetMenu(fileName = "PrototypeLoockup", menuName = "Core/Create Prototype Loockup")]
    public class PrototypeLoockup : ScriptableObject
    {
        [SerializeField] private PrototypeGroup[] groups = new PrototypeGroup[0];

        public Dictionary<int, Object> Pairs = new Dictionary<int, Object>();

        public void Initialize()
        {
            for (var i = 0; i < groups.Length; i++)
            {
                var group = groups[i];
                for (var j = 0; j < group.Pairs.Length; j++)
                {
                    var pair = group.Pairs[j];
                    Pairs.Add(pair.PrototypeId, pair.Prototype);
                }
            }
        }
    }
}
