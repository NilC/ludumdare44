﻿using UnityEngine;
using System.Collections.Generic;

namespace LD44
{

    public class EntityFactory
    {
        private static readonly Vector3 HiddenPosition = new Vector3(-100.0f, -100.0f, 0.0f);

        private EntitySystem entitySystem = null;
        private PrototypeLoockup prototypesLoockup = null;
        private PoolCapacity[] poolCapacities = null;

        private Dictionary<int, GameObjectPool> pools = new Dictionary<int, GameObjectPool>();

        public EntityFactory(PrototypeLoockup prototypesLoockup, PoolCapacity[] poolCapacities, GameObject poolsRoot)
        {
            this.prototypesLoockup = prototypesLoockup;
            this.poolCapacities = poolCapacities;

            CreatePools(poolsRoot);
        }

        public int Create(int prototypeId, Vector3 position)
        {
            var pool = pools[prototypeId];
            var itemIndex = pool.RequestItem();

            var item = pool.Items[itemIndex];

            item.gameObject.SetActive(true);
            for (int i = 0; i < item.RelatedSystems.Length; i++)
            {
                item.RelatedSystems[i].ResetComponent(item.EntityId);
            }

            item.transform.position = position;

            return item.EntityId;
        }

        public void Destroy(EntityComponent entityComponent)
        {
            var pool = pools[entityComponent.PrototypeId];

            entityComponent.gameObject.SetActive(false);
            entityComponent.transform.SetParent(pool.RootTransform);
            entityComponent.transform.localPosition = HiddenPosition;

            pool.RecycleItemByIndex(entityComponent.PoolLink.ItemIndex);
        }

        public int RegisterEntity(EntityComponent entityComponent)
        {
            var entityId = entityComponent.EntityId;

            var pool = pools[entityComponent.PrototypeId];
            var itemId = pool.LinkItem(entityComponent);

            entityComponent.PoolLink = new PoolLink(pool, itemId);

            return entityId;
        }

        private void CreatePools(GameObject poolsRoot)
        {
            foreach (var prototypePair in prototypesLoockup.Pairs)
            {
                var prototypeId = prototypePair.Key;

                var poolCreationSettings = new GameObjectPoolCreationSettings
                {
                    PrototypeId = prototypeId,
                    PoolsRoot = poolsRoot,
                    NamePostfix = GetPoolNamePostfix(prototypeId),
                    ConstructMethod = ConstructEntity,
                    Capacity = GetCapacity(prototypeId),
                    GrowingCount = 1
                };

                var pool = new GameObjectPool(poolCreationSettings);
                pools.Add(prototypeId, pool);
            }
        }

        private int GetCapacity(int prototypeId)
        {
            for (var i = 0; i < poolCapacities.Length; i++)
            {
                var poolCapacity = poolCapacities[i];
                if (poolCapacity.PrototypeId == prototypeId)
                {
                    return poolCapacity.Capacity;
                }
            }

            throw new UnityException($"Can not find capacity for prototype id {prototypeId}");
        }

        private string GetPoolNamePostfix(int prototypeId)
        {
            return prototypesLoockup.Pairs[prototypeId].name;
        }

        private EntityComponent ConstructEntity(int prototypeId, Transform parent)
        {
            var masterObject = prototypesLoockup.Pairs[prototypeId];
            var poolObject = Object.Instantiate(masterObject, HiddenPosition, Quaternion.identity, parent) as GameObject;

            var entityComponent = poolObject.GetComponent<EntityComponent>();
#if UNITY_EDITOR
            if (entityComponent == null)
            {
                throw new UnityException("Can not pool component");
            }
#endif

            IComponent[] components = poolObject.GetComponents<IComponent>();

            entityComponent.RelatedSystems = new IComponentSystem[components.Length];
            for (var i = 0; i < components.Length; i++)
            {
                entityComponent.RelatedSystems[i] = components[i].GetRelatedSystem();
            }

            entityComponent.gameObject.SetActive(false);

            return poolObject.GetComponent<EntityComponent>();
        }
    }
}