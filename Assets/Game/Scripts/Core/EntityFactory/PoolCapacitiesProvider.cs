﻿using UnityEngine;

namespace LD44
{
    public class PoolCapacitiesProvider : MonoBehaviour
    {
        [SerializeField] private PoolCapacity[] poolCapacities = null;

        public PoolCapacity[] GetPoolCapacities()
        {
            return poolCapacities;
        }
    }
}