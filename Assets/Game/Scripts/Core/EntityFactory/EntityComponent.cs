﻿using UnityEngine;

namespace LD44
{
    public class EntityComponent : MonoBehaviour, IComponent
    {
        //When object precreate on scene - pool link is empty
        //So we can get it by PrototypeId
        public int PrototypeId;

        [HideInInspector]
        public PoolLink PoolLink;

        public int EntityId { private set; get; }

        public IComponentSystem[] RelatedSystems;

        public void Awake()
        {
            EntityId = gameObject.GetEntityId();
        }

        public void OnEnable()
        {
            Systems.Entities.AddComponent(gameObject.GetEntityId(), this);
        }

        public void OnDisable()
        {
            Systems.Entities.DisableComponent(gameObject.GetEntityId());
        }

        public IComponentSystem GetRelatedSystem()
        {
            return Systems.Entities;
        }
    }
}
