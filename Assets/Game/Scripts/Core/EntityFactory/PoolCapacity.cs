﻿using System;

namespace LD44
{
    [Serializable]
    public struct PoolCapacity
    {
        public int PrototypeId;
        public int Capacity;

        public PoolCapacity(int prototypeId, int capacity)
        {
            PrototypeId = prototypeId;
            Capacity = capacity;
        }
    }
}