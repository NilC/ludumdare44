﻿using System.Collections.Generic;
using UnityEngine;

namespace LD44
{
    public struct CreateRequest
    {
        public int PrototypeId;
        public Vector3 Position;
    }

    public class EntitySystem : ComponentSystem<EntityComponent>
    {
        private EntityFactory entityFactory = null;

        private List<CreateRequest> createRequests = null;
        private HashSet<int> destroyRequests = null;

        public override void Initialize()
        {
            base.Initialize();

            WarmupRequestsContainers();
            RegisterExistingEntities();
        }

        public override void Run()
        {
            foreach (var entityId in destroyRequests)
            {
                var component = GetComponent(entityId);
                //Debug.Log($"Destroy: {component}");
                entityFactory.Destroy(component);
            }
            destroyRequests.Clear();

            for (var i = 0; i < createRequests.Count; i++)
            {
                var request = createRequests[i];
                //Debug.Log($"Create: {request.PrototypeId}");
                entityFactory.Create(request.PrototypeId, request.Position);
            }
            createRequests.Clear();
        }

        public void AddCreateRequest(CreateRequest createRequest)
        {
            createRequests.Add(createRequest);
        }

        public void AddDestroyRequest(int entityId)
        {
            destroyRequests.Add(entityId);
        }

        public void SetEntityFactory(EntityFactory entityFactory)
        {
            this.entityFactory = entityFactory;
        }

        private void WarmupRequestsContainers()
        {
            createRequests = new List<CreateRequest>(30);
            destroyRequests = new HashSet<int>();
            for (var i = 0; i < createRequests.Capacity; i++)
            {
                destroyRequests.Add(i);
            }
            destroyRequests.Clear();
        }

        private void RegisterExistingEntities()
        {
            var entityComponents = Object.FindObjectsOfType<EntityComponent>();
            for (int i = 0; i < entityComponents.Length; i++)
            {
                entityFactory.RegisterEntity(entityComponents[i]);
            }
        }
    }

    public partial class Systems
    {
        public static EntitySystem Entities = new EntitySystem();
    }
}
