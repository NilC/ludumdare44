﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.Linq;

namespace LD44.Editor
{
    [CustomEditor(typeof(PoolCapacitiesProvider), true)]
    public class PoolCapacitiesProviderEditor : CustomInspectorEditor
    {
        private const string PoolCapacities = "poolCapacities";
        private const string PrototypeId = "PrototypeId";
        private const string Prototype = "Prototype";
        private const string Capacity = "Capacity";

        private const string Title = "Pool Capacities";

        private EntityComponent[] poolComponents;

        private TableView tableView = null;

        private void OnEnable()
        {
            //ToDo: Better flow for get assets needed
            poolComponents = Resources.LoadAll<EntityComponent>(string.Empty);

            var poolCapacitiesProperty = serializedObject.FindProperty(PoolCapacities);
            tableView = new TableView(serializedObject, poolCapacitiesProperty, Title,
                new TableColumn[]
                {
                    new TableColumn
                    {
                        Header = PrototypeId.ToCamelCaseWithSpaces(),
                        WidthFactor = 0.3333f,
                        FieldName = PrototypeId
                    },
                    new TableColumn
                    {
                        Header = Prototype,
                        WidthFactor = 0.3333f,
                        CustomDrawCallback = (Rect rect, SerializedProperty element) =>
                        {
                            var prototypeId = element.FindPropertyRelative(PrototypeId).intValue;
                            var prototypeName = "Not found";
                            var poolComponent = poolComponents.FirstOrDefault(x => x.PrototypeId == prototypeId);
                            if (poolComponent != null)
                            {
                                prototypeName = poolComponent.name;
                            }
                            EditorGUI.LabelField(rect, prototypeName);
                        }
                    },
                    new TableColumn
                    {
                        Header = Capacity.ToCamelCaseWithSpaces(),
                        WidthFactor = 0.3333f,
                        FieldName = Capacity
                    }
                },
                TableFoldoutMode.NotFoldout,
                TableRowDragableMode.Dragable,
                TableRemoveMode.WithoutRemove,
                null,
                null);
        }

        private void OnDisable()
        {
            poolComponents = null;
            Resources.UnloadUnusedAssets();
        }

        public override void OnInspectorGUI()
        {
            DrawScriptFieldFromMonoBehaviour();

            serializedObject.Update();

            tableView.Draw();

            serializedObject.ApplyModifiedProperties();
        }
    }
}

#endif