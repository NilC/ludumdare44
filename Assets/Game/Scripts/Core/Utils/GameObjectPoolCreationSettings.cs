﻿using System;
using UnityEngine;

namespace LD44
{
    public sealed class GameObjectPoolCreationSettings
    {
        public int PrototypeId;
        public GameObject PoolsRoot;
        public string NamePostfix;
        public Func<int, Transform, EntityComponent> ConstructMethod;
        public int Capacity;
        public int GrowingCount;
    }
}