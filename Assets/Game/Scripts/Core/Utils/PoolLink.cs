﻿namespace LD44
{  
    public struct PoolLink
    {
        public IPool Pool;
        public int ItemIndex;

        public PoolLink(IPool pool, int itemIndex)
        {
            Pool = pool;
            ItemIndex = itemIndex;
        }
    }
}