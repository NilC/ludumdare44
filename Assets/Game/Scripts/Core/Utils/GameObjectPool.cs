﻿using System;
using UnityEngine;

namespace LD44
{
    public sealed class GameObjectPool : IPool
    {
        private const int MinSize = 8;

        private readonly int prototypeId = 0;

        private readonly GameObject poolRoot = null;
        private readonly Transform poolRootTransform = null;

        private readonly Func<int, Transform, EntityComponent> constructMethod = null;
        private readonly int growingCount = 0;

        public EntityComponent[] Items = new EntityComponent[MinSize];

        private int itemsCount;

        private int[] requestedIndicies = new int[MinSize];
        private int requestedIndiciesCount;

        public Transform RootTransform => poolRootTransform;

        public GameObjectPool(GameObjectPoolCreationSettings creationSettings)
        {
            prototypeId = creationSettings.PrototypeId;
            poolRoot = new GameObject(string.Format("Pool {0}", creationSettings.NamePostfix));
            poolRoot.transform.SetParent(creationSettings.PoolsRoot.transform);
            poolRootTransform = poolRoot.transform;

            constructMethod = creationSettings.ConstructMethod;
            growingCount = creationSettings.GrowingCount;

            Create(creationSettings.Capacity);
        }

        public int RequestItem()
        {
            int index;
            if (requestedIndiciesCount > 0)
            {
                requestedIndiciesCount--;
                index = requestedIndicies[requestedIndiciesCount];
            }
            else
            {
                index = itemsCount;
                InitItemAtEnd(constructMethod(prototypeId, poolRootTransform));
            }
            return index;
        }

        public void RecycleItemByIndex(int index)
        {
            if (requestedIndiciesCount == requestedIndicies.Length)
            {
                Array.Resize(ref requestedIndicies, requestedIndiciesCount << 1);
            }
            requestedIndicies[requestedIndiciesCount] = index;
            requestedIndiciesCount++;
        }

        public int LinkItem(EntityComponent entityComponent)
        {
            var index = itemsCount;
            InitItemAtEnd(entityComponent);
            return index;
        }

        private void Create(int capacity)
        {
            Array.Resize(ref Items, capacity);
            for (int i = 0; i < Items.Length; i++)
            {
                Items[i] = constructMethod(prototypeId, poolRootTransform);
            }
            itemsCount = Items.Length;

            Array.Resize(ref requestedIndicies, capacity);
            for (int i = 0; i < requestedIndicies.Length; i++)
            {
                requestedIndicies[i] = i;
            }
            requestedIndiciesCount = requestedIndicies.Length;
        }

        private void InitItemAtEnd(EntityComponent entityComponent)
        {
            if (itemsCount == Items.Length)
            {
                Array.Resize(ref Items, itemsCount << 1);
            }
            Items[itemsCount] = entityComponent;
            itemsCount++;
        }
    }
}