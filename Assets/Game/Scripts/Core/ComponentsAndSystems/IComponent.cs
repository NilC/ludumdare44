﻿namespace LD44
{
    public interface IComponent
    {
        int EntityId { get; }
        IComponentSystem GetRelatedSystem();
    }
}