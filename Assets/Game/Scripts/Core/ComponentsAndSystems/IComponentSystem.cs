﻿namespace LD44
{
    public interface IComponentSystem : ISystem
    {
        void ResetComponent(int entityId);
        int GetComponentsCount();
    }
}