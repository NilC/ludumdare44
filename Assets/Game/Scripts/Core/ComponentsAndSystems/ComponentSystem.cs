﻿using System.Collections.Generic;

namespace LD44
{
    public abstract class ComponentSystem<TComponentType> : IConcreteComponentSystem<TComponentType> where TComponentType : IComponent
    {
        protected Dictionary<int, TComponentType> Components = new Dictionary<int, TComponentType>();

        public bool IsNeedToRun { get; set; } = true;

        public virtual void Initialize()
        {
        }

        public virtual void Shutdown()
        {
        }

        public virtual void Run()
        {
        }

        public virtual void FixedRun()
        {
        }

#if UNITY_EDITOR
        public virtual void DrawGizmos()
        {
        }
#endif

        public virtual void AddComponent(int entityId, TComponentType component)
        {
            Components.Add(entityId, component);
        }

        public virtual void DisableComponent(int entityId)
        {
            Components.Remove(entityId);
        }

        public virtual void ResetComponent(int entityId)
        {
        }

        public virtual TComponentType GetComponent(int entityId)
        {
            return Components[entityId];
        }

        public int GetComponentsCount()
        {
            return Components.Count;
        }
    }
}