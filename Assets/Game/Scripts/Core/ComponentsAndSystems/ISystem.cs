﻿namespace LD44
{
    public interface ISystem
    {
        bool IsNeedToRun { get; set; }

        void Initialize();
        void Shutdown();
        void Run();
        void FixedRun();

#if UNITY_EDITOR
        void DrawGizmos();
#endif
    }
}