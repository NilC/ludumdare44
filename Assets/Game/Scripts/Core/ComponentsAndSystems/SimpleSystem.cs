﻿
namespace LD44
{
    public abstract class SimpleSystem : ISystem
    {
        public bool IsNeedToRun { get; set; } = true;

        public virtual void Initialize()
        {
        }

        public virtual void Shutdown()
        {
        }

        public virtual void Run()
        {
        }

        public virtual void FixedRun()
        {
        }

#if UNITY_EDITOR
        public virtual void DrawGizmos()
        {
        }
#endif
    }
}