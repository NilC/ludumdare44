﻿namespace LD44
{
    public interface IConcreteComponentSystem<TComponentType> : IComponentSystem where TComponentType : IComponent
    {
        void AddComponent(int entityId, TComponentType component);
        void DisableComponent(int entityId);
        TComponentType GetComponent(int entityId);
    }
}