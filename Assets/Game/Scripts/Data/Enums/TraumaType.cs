﻿namespace LD44
{
    public enum TraumaType
    {
        None = 0,
        Head = 1,
        BodyPiercing = 2,
        BodyDrop = 3
    }
}