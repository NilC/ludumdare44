﻿namespace LD44
{
    public enum MustWearItemType
    {
        None = 0,
        Helmet = 1,
        Pillow = 2,
    }
}