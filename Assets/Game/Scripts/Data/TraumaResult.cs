﻿using System;
using UnityEngine;

namespace LD44
{
    [Serializable]
    public struct TraumaResult
    {
        public TraumaType TraumaType;
        public int Compensation;
        public MustWearItemType MustWearItemType;
        public Sprite Sprite;
    }
}