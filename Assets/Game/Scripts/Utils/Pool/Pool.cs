﻿using System;

namespace LD44
{
    public sealed class Pool<T> : IPool where T : class, new()
    {
        private const int MinSize = 8;

        public static readonly Pool<T> Instance = new Pool<T>();

        public T[] Items = new T[MinSize];

        private int itemsCount;

        private int[] requestedIndicies = new int[MinSize];
        private int requestedIndiciesCount;

        private Func<T> constructMethod;

        /// <summary>
        /// Request an item in pool
        /// </summary>
        /// <returns>Index of Items</returns>
        public int RequestItem()
        {
            int index;
            if (requestedIndiciesCount > 0)
            {
                requestedIndiciesCount--;
                index = requestedIndicies[requestedIndiciesCount];
            }
            else
            {
                index = itemsCount;
                if (itemsCount == Items.Length)
                {
                    Array.Resize(ref Items, itemsCount << 1);
                }
                Items[itemsCount] = constructMethod != null ? constructMethod() : (T)Activator.CreateInstance(typeof(T));
                itemsCount++;
            }
            return index;
        }

        public void RecycleItemByIndex(int index)
        {
            if (requestedIndiciesCount == requestedIndicies.Length)
            {
                Array.Resize(ref requestedIndicies, requestedIndiciesCount << 1);
            }
            requestedIndicies[requestedIndiciesCount] = index;
            requestedIndiciesCount++;
        }

        public void SetConstructMethod(Func<T> constructMethod)
        {
            this.constructMethod = constructMethod;
        }

        public void SetCapacity(int capacity)
        {
            if (capacity < Items.Length)
            {
                return;
            }
            Array.Resize(ref Items, capacity);
        }

        public void SetCapacityWithWarmUp(int capacity)
        {
            Array.Resize(ref Items, capacity);
            for (int i = 0; i < Items.Length; i++)
            {
                Items[i] = constructMethod != null ? constructMethod() : (T)Activator.CreateInstance(typeof(T));
            }
            itemsCount = Items.Length;

            Array.Resize(ref requestedIndicies, capacity);
            for (int i = 0; i < requestedIndicies.Length; i++)
            {
                requestedIndicies[i] = i;
            }
            requestedIndiciesCount = requestedIndicies.Length;
        }

        public void Shrink()
        {
            int capacity;
            capacity = itemsCount < MinSize ? MinSize : MathHelper.GetPowerOfTwoSize(itemsCount);
            if (Items.Length != capacity)
            {
                Array.Resize(ref Items, capacity);
            }
            capacity = requestedIndiciesCount < MinSize ? MinSize : MathHelper.GetPowerOfTwoSize(requestedIndiciesCount);
            if (requestedIndicies.Length != capacity)
            {
                Array.Resize(ref requestedIndicies, capacity);
            }
        }
    }
}