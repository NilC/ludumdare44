﻿using System.Text;

namespace LD44
{
    public static class StringExtension
    {
        public static string ToCamelCaseWithSpaces(this string sourceString)
        {
            StringBuilder result = new StringBuilder((int)(sourceString.Length * 1.5f));
            for (int i = 0; i < sourceString.Length; i++)
            {
                char letter = sourceString[i];
                if (i > 0 && char.IsUpper(letter))
                {
                    result.AppendFormat(" {0}", letter);
                }
                else
                {
                    result.Append(letter);
                }
            }

            return result.ToString();
        }
    }
}