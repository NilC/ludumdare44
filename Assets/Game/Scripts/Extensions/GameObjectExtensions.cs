﻿using UnityEngine;

public static class GameObjectExtensions
{
    public static int GetEntityId(this GameObject gameObject)
    {
        return gameObject.GetInstanceID();
    }
}
