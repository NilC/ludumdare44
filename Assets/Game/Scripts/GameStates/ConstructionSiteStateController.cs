﻿using System.Collections.Generic;
using UnityEngine;

namespace LD44
{
    public class ConstructionSiteStateController : GameStateController
    {
        [SerializeField] private GameObject poolsRoot = null;
        List<ISystem> gameStateSystems = new List<ISystem>(10);

        public override void Initialize(PrototypeLoockup prototypesLoockup, PoolCapacity[] poolCapacities)
        {
            EntityFactory = new EntityFactory(prototypesLoockup, poolCapacities, poolsRoot);
            Systems.Entities.SetEntityFactory(EntityFactory);

            gameStateSystems.Add(Systems.CustomId);
            gameStateSystems.Add(Systems.Input);
            gameStateSystems.Add(Systems.Heroes);
            gameStateSystems.Add(Systems.FallingBricks);
            gameStateSystems.Add(Systems.LinearMovement);
            gameStateSystems.Add(Systems.MovementToPoint);
            gameStateSystems.Add(Systems.RigitbodyPositioning);
            gameStateSystems.Add(Systems.Cameras);
            gameStateSystems.Add(Systems.Lifetime);
            gameStateSystems.Add(Systems.Entities);

            foreach (var gameStateSystem in gameStateSystems)
            {
                gameStateSystem.IsNeedToRun = true;
                gameStateSystem.Initialize();
            }
        }

        public override void Shutdown()
        {
            foreach (var gameStateSystem in gameStateSystems)
            {
                gameStateSystem.Shutdown();
            }
        }

        public override void Run()
        {
            foreach (var gameStateSystem in gameStateSystems)
            {
                if (gameStateSystem.IsNeedToRun)
                {
                    gameStateSystem.Run();
                }
            }
        }

        public override void FixedRun()
        {
            foreach (var gameStateSystem in gameStateSystems)
            {
                if (gameStateSystem.IsNeedToRun)
                {
                    gameStateSystem.FixedRun();
                }
            }
        }

#if UNITY_EDITOR
        public override void DrawGizmos()
        {
            foreach (var gameStateSystem in gameStateSystems)
            {
                gameStateSystem.DrawGizmos();
            }
        }
#endif
    }
}