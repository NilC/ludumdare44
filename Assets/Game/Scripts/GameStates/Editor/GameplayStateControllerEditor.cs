﻿using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace LD44.Editor
{
    public class GameplayStateControllerDebugWindow : EditorWindow
    {
        ConstructionSiteStateController controller = null;

        [MenuItem("Window/Debug Systems")]
        public static void OpenWindow()
        {
            GameplayStateControllerDebugWindow window = GetWindow<GameplayStateControllerDebugWindow>();
            window.titleContent = new GUIContent("Debug systems");
        }

        private void OnInspectorUpdate()
        {
            Repaint();
        }

        private void OnGUI()
        {
            controller = FindObjectOfType<ConstructionSiteStateController>();
            if (controller == null)
            {
                return;
            }

            FieldInfo roomObjectTypeFieldInfo = controller.GetType().GetField("gameStateSystems", BindingFlags.NonPublic | BindingFlags.Instance);
            List<ISystem> systems = (List<ISystem>)roomObjectTypeFieldInfo.GetValue(controller);

            foreach (var system in systems)
            {
                GUILayout.BeginHorizontal();
                var componentSystem = system as IComponentSystem;
                if (componentSystem != null)
                {
                    EditorGUILayout.LabelField($"{componentSystem}: {componentSystem.GetComponentsCount()}");
                }
                GUILayout.EndHorizontal();
            }
        }
    }
}
